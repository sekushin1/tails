# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Aleix Vidal i Gaya <aleix.vidal@gmail.com>, 2014
# Assumpta <assumptaanglada@gmail.com>, 2014
# Eloi García i Fargas, 2014
# Guillem Arias Fauste <inactive+Mr_SpeedArt@transifex.com>, 2016
# Humbert <humbert.costas@gmail.com>, 2014
# laia_, 2014-2016
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-06-05 11:57+0200\n"
"PO-Revision-Date: 2017-05-26 14:47+0000\n"
"Last-Translator: carolyn <carolyn@anhalt.org>\n"
"Language-Team: Catalan (http://www.transifex.com/otf/torproject/language/"
"ca/)\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:39
msgid "Tor is ready"
msgstr "El Tor està preparat"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:40
msgid "You can now access the Internet."
msgstr "Ara ja podeu accedir a Internet."

#: config/chroot_local-includes/etc/whisperback/config.py:65
#, python-format
msgid ""
"<h1>Help us fix your bug!</h1>\n"
"<p>Read <a href=\"%s\">our bug reporting instructions</a>.</p>\n"
"<p><strong>Do not include more personal information than\n"
"needed!</strong></p>\n"
"<h2>About giving us an email address</h2>\n"
"<p>\n"
"Giving us an email address allows us to contact you to clarify the problem. "
"This\n"
"is needed for the vast majority of the reports we receive as most reports\n"
"without any contact information are useless. On the other hand it also "
"provides\n"
"an opportunity for eavesdroppers, like your email or Internet provider, to\n"
"confirm that you are using Tails.\n"
"</p>\n"
msgstr ""
"<h1>Ajudeu-nos a arreglar aquest error!</h1>\n"
"<p>Llegeix <a href=\"%s\">les nostres instruccions de comunicació d'errors</"
"a>.</p>\n"
"<p><strong>No incloeu més informació personal \n"
"que la necessària!</strong></p>\n"
"<h2>Sobre el fet de donar-nos una adreça de correu</h2>\n"
"<p>\n"
"Donar-nos una adreça de correu ens permet contactar amb vosaltres per "
"aclarir el\n"
"problema. Això és necessari per la majoria d'informes que rebem, ja que la "
"major part\n"
"d'informes sense informació de contacte són inútils. D'altra banda, aquest "
"fet també dóna\n"
"una oportunitat als espies o al vostre proveïdor d'internet per confirmar "
"que esteu usant el Tails.\n"
"</p> \n"

#: config/chroot_local-includes/usr/local/bin/electrum:17
msgid "Persistence is disabled for Electrum"
msgstr "La Persistència ha estat deshabilitada per l'Electrum"

#: config/chroot_local-includes/usr/local/bin/electrum:19
msgid ""
"When you reboot Tails, all of Electrum's data will be lost, including your "
"Bitcoin wallet. It is strongly recommended to only run Electrum when its "
"persistence feature is activated."
msgstr ""
"Quan reinicieu el Tails, totes les dades de l'Electrum es perdran, inclòs el "
"moneder de Bitcoins. És molt recomanable que només useu l'Electrum quan la "
"seva propietat de persistència estigui activada. "

#: config/chroot_local-includes/usr/local/bin/electrum:21
msgid "Do you want to start Electrum anyway?"
msgstr "Voleu iniciar l'Electrum igualment?"

#: config/chroot_local-includes/usr/local/bin/electrum:23
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:41
msgid "_Launch"
msgstr "_Executa"

#: config/chroot_local-includes/usr/local/bin/electrum:24
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:42
msgid "_Exit"
msgstr "_Surt"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/shutdown-helper@tails.boum.org/extension.js:71
msgid "Restart"
msgstr "Reinicia"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/shutdown-helper@tails.boum.org/extension.js:74
msgid "Power Off"
msgstr "Apaga"

#: config/chroot_local-includes/usr/local/bin/tails-about:22
#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:1
msgid "Tails"
msgstr "Tails"

#: config/chroot_local-includes/usr/local/bin/tails-about:25
#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:1
msgid "About Tails"
msgstr "Quant a Tails"

#: config/chroot_local-includes/usr/local/bin/tails-about:35
msgid "The Amnesic Incognito Live System"
msgstr "The Amnesic Incognito Live System"

#: config/chroot_local-includes/usr/local/bin/tails-about:36
#, python-format
msgid ""
"Build information:\n"
"%s"
msgstr ""
"Construeix informació:\n"
"%s"

#: config/chroot_local-includes/usr/local/bin/tails-about:54
msgid "not available"
msgstr "no disponible"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:147
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:162
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:166
msgid "Your additional software"
msgstr "Programari addicional"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:148
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:167
msgid ""
"The upgrade failed. This might be due to a network problem. Please check "
"your network connection, try to restart Tails, or read the system log to "
"understand better the problem."
msgstr ""
"L'actualització ha fallat. És possible que hi hagi un problema a la xarxa. "
"Comproveu la  connexió de la xarxa, intenteu reiniciar Tails, o llegiu el "
"registre del sistema per entendre millor el problema. "

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:163
msgid "The upgrade was successful."
msgstr "La millora s'ha fet satisfactòriament."

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:52
msgid "Synchronizing the system's clock"
msgstr "Sincronitzant el rellotge del sistema"

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:53
msgid ""
"Tor needs an accurate clock to work properly, especially for Hidden "
"Services. Please wait..."
msgstr ""
"Tor necessita un rellotge precís per funcionar correctament, especialment "
"per als serveis ocults. Espereu...."

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:87
msgid "Failed to synchronize the clock!"
msgstr "Error en sincronitzar el rellotge"

#: config/chroot_local-includes/usr/local/bin/tails-security-check:124
msgid "This version of Tails has known security issues:"
msgstr "Aquesta versió de Tails té problemes de seguretat coneguts:"

#: config/chroot_local-includes/usr/local/bin/tails-security-check:134
msgid "Known security issues"
msgstr "Problemes de seguretat coneguts"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:52
#, sh-format
msgid "Network card ${nic} disabled"
msgstr "La targeta de xarxa ${nic} està deshabilitada"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:53
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}) so it is "
"temporarily disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"El falsejament de la identitat de l'adreça MAC ha fallat per la targeta de "
"xarxa ${nic_name} (${nic}) i està temporalment desactivada. \n"
"Potser preferireu reiniciar el Tails i desactivar el falsejament de l'adreça "
"MAC "

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:62
msgid "All networking disabled"
msgstr "Totes les xarxes estan deshabilitades"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:63
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}). The error "
"recovery also failed so all networking is disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"El falsejament de la identitat de l'adreça MAC ha fallat per la targeta de "
"xarxa ${nic_name} (${nic}). També ha fallat la recuperació d'errors i tot el "
"treball en xarxa està desactivat.\n"
"Potser preferireu reiniciar el Tails i desactivar el falsejament de l'adreça "
"MAC "

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:24
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:27
msgid "error:"
msgstr "Hi ha hagut un error:"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:25
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:28
msgid "Error"
msgstr "Error"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:45
msgid ""
"<b>Not enough memory available to check for upgrades.</b>\n"
"\n"
"Make sure this system satisfies the requirements for running Tails.\n"
"See file:///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"Try to restart Tails to check for upgrades again.\n"
"\n"
"Or do a manual upgrade.\n"
"See https://tails.boum.org/doc/first_steps/upgrade#manual"
msgstr ""
"<b>No hi ha memòria suficient per cercar actualitzacions.</b>\n"
"\n"
"Assegureu-vos que aquest sistema compleix els requisits tècnics per a "
"executar Tails. \n"
"Vegeu file:///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"Intenteu reiniciar Tails per cercar actualitzacions una altra vegada. \n"
"\n"
"O feu una actualització manual. \n"
"Vegeu https://tails.boum.org/doc/first_steps/upgrade#manual"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:71
msgid "Warning: virtual machine detected!"
msgstr "Alerta: S'ha detectat una màquina virtual."

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:73
msgid ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails."
msgstr ""
"Tant el sistema operatiu instal·lat com el programari de virtualització "
"estan preparats per fer un seguiment del que feu a Tails. "

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:76
msgid "Warning: non-free virtual machine detected!"
msgstr "Avís: S'ha detectat una màquina virtual de propietat"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:78
msgid ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails. Only free software can be considered "
"trustworthy, for both the host operating system and the virtualization "
"software."
msgstr ""
"Tant el sistema operatiu instal·lat com el programari de virtualització "
"poden monitoritzar el que feu a Tails. Només el programari lliure pot ésser "
"considerat de confiança, tant pel sistema operatiu com per al programari de "
"virtualització."

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:83
msgid "Learn more"
msgstr "Apreneu-ne més"

#: config/chroot_local-includes/usr/local/bin/tor-browser:43
msgid "Tor is not ready"
msgstr "El Tor no està preparat"

#: config/chroot_local-includes/usr/local/bin/tor-browser:44
msgid "Tor is not ready. Start Tor Browser anyway?"
msgstr "El Tor no està preparat. Voleu iniciar el navegador Tor igualment?"

#: config/chroot_local-includes/usr/local/bin/tor-browser:45
msgid "Start Tor Browser"
msgstr "Inicia el navegador Tor"

#: config/chroot_local-includes/usr/local/bin/tor-browser:46
msgid "Cancel"
msgstr "Cancel·la"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:38
msgid "Do you really want to launch the Unsafe Browser?"
msgstr "Segur que voleu executar el navegador insegur?"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:40
msgid ""
"Network activity within the Unsafe Browser is <b>not anonymous</b>.\\nOnly "
"use the Unsafe Browser if necessary, for example\\nif you have to login or "
"register to activate your Internet connection."
msgstr ""
"L'activitat de la xarxa amb el navegador insegur <b>no és anònima</b>. "
"\\nUtilitzeu només el navegador insegur si és necessari, per exemple\\nsi "
"heu d'iniciar sessió o registrar-vos per activar la connexió a internet. "

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:51
msgid "Starting the Unsafe Browser..."
msgstr "Iniciant el Navegador Insegur..."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:52
msgid "This may take a while, so please be patient."
msgstr "Això pot trigar una estona, espereu."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:57
msgid "Shutting down the Unsafe Browser..."
msgstr "Apagant el Navegador Insegur"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:58
msgid ""
"This may take a while, and you may not restart the Unsafe Browser until it "
"is properly shut down."
msgstr ""
"Això pot tardar una estona, no reinicieu el navegador insegur fins que no "
"s'hagi apagat correctament."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:70
msgid "Failed to restart Tor."
msgstr "Hi ha hagut un error mentre es reiniciava Tor."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:84
#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:1
msgid "Unsafe Browser"
msgstr "Navegador Insegur"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:92
msgid ""
"Another Unsafe Browser is currently running, or being cleaned up. Please "
"retry in a while."
msgstr ""
"Un altre navegador insegur està essent executat o netejat. Proveu-ho d'aquí "
"una estona. "

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:104
msgid ""
"NetworkManager passed us garbage data when trying to deduce the clearnet DNS "
"server."
msgstr ""
"El gestor de la xarxa ens ha donat dades brossa quan intentàvem deduir el "
"servidor DNS."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:114
msgid ""
"No DNS server was obtained through DHCP or manually configured in "
"NetworkManager."
msgstr ""
"No s'ha obtingut cap servidor DNS a través del DHCP o configurat manualment "
"al NetworkManager. "

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:122
msgid "Failed to setup chroot."
msgstr "S'ha fallat en configurar chroot."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:128
msgid "Failed to configure browser."
msgstr "S'ha fallat en configurar el navegador."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:133
msgid "Failed to run browser."
msgstr "S'ha fallat en executar el navegador."

#: ../config/chroot_local-includes/etc/skel/Desktop/Report_an_error.desktop.in.h:1
msgid "Report an error"
msgstr "Reporta un error"

#: ../config/chroot_local-includes/etc/skel/Desktop/tails-documentation.desktop.in.h:1
#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:1
msgid "Tails documentation"
msgstr "Documentacio de Tails"

#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:2
msgid "Learn how to use Tails"
msgstr "Com usar el Tails"

#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:2
msgid "Learn more about Tails"
msgstr "Aprèn més sobre Tails"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:1
msgid "Tor Browser"
msgstr "Navegador Tor"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:2
msgid "Anonymous Web Browser"
msgstr "Navegador web anònim"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:2
msgid "Browse the World Wide Web without anonymity"
msgstr "Navegar per la World Wide Web sense anonimitzar"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:3
msgid "Unsafe Web Browser"
msgstr "Navegador Web Insegur"

#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:2
msgid "Tails specific tools"
msgstr "Eines específiques de Tails"
